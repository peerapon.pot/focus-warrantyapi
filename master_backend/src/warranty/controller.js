const wrModel = require('./model')




const warrantycode = () => async (req, res, next) => {
    // let { decode } = req.body
    try {
        let respon = await wrModel.codegenerate(req.body.decode)
        //console.log(respon)
        respon = await checkUsedcode(respon)

        req.respon = respon
        req.message = ''
        req.status = 200
    } catch (error) {
        console.log(error)
        req.respon = []
        req.message = error
        req.status = 500
    }
    next()
}
const update = () => async (req, res, next) => {
    let { decode } = req.body
    try {
        let respon = await wrModel.updatecodegenerate(decode)

        //console.log(respon)
        // respon = await checkUsedcode(respon)

        req.respon = decode
        req.message = 'update success'
        req.status = 200
    } catch (error) {
        console.log(error)
        req.respon = []
        req.message = error
        req.status = 500
    }
    next()
}




const checkUsedcode = async (obj) => {
    let respon

    if (obj.length === 0) {
        respon = { respon: [], mess: 'ไม่พบ QR ดังกล่าว', status: 201 }
        return respon
    }
    else if (obj[0].status === 'active' && obj[0].status_check === '') {

        respon = { respon: obj, mess: 'สามารถใช้งานได้', status: 200 }
        return respon
    }
    else if (obj[0].status === 'noneactive') {
        respon = { respon: [], mess: 'ใช้งานไม่ได้', status: 202 }
        return respon
    }
    else if (obj[0].status === 'active' && obj[0].status_check === 'ACTIVE') {
        respon = { respon: [], mess: 'code นี้ถูกใช้งานไปแล้ว', status: 203 }
        return respon
    }

    else {
        respon = { respon: obj, mess: 'กรุณาลองใหม่อีกครั้ง', status: 204 }
        return respon
    }


}






module.exports = {
    warrantycode,
    update
    // insert
}