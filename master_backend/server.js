const http = require('http');
const app = require('./index');

const port = process.env.PORT || 4333;

const server = http.createServer(app);

server.listen(port, () => {
    console.log("Server is ready,PORT use " + port)
});

//test dev