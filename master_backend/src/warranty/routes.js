const express = require('express');

const router = express.Router();
const controller = require('./controller')

router.post('/code',

    controller.warrantycode(),
    (req, res) => {
        res.status(req.status).json({ respon: req.respon, message: req.message })
    })
router.post('/update',

    controller.update(),
    (req, res) => {
        res.status(req.status).json({ respon: req.respon, message: req.message })
    })
module.exports = router;