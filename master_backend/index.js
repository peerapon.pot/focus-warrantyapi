const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const createApi = require('./src/index');
const cors = require('cors');



require('./service/dbconnect'); // use knex

const corsOption = {
    optionsSuccessStatus: 200,
    preflightContinue: true,
    credentials: true
}
app.use(cors(corsOption));
////Body parser 
app.use(morgan('dev'));
// app.use('/', express.static(__dirname + ''));
// app.use('/upload', express.static('upload'));
app.use(bodyParser.urlencoded({ extended: true, limit: 1024 * 1024 * 20, type: 'application/x-www-form-urlencoded' }));
app.use(bodyParser.json({ limit: 1024 * 1024 * 2000, type: 'application/json' }));

createApi(app)

app.get('/', (req, res) => {
    res.render('index');
})
app.post('/test', (req, res) => {
    console.log(req.body)
})
//Error url send//
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
})


module.exports = app;
